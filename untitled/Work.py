# 3
import re

print("3")
string1 = "<[{()}]>"


def check(string):
    k1 = 0
    k2 = 0
    k3 = 0
    k4 = 0
    for i in string:
        if i == "(":
            k1 += 1
        if i == "{":
            k2 += 1
        if i == "[":
            k3 += 1
        if i == "<":
            k4 += 1
        if i == ")":
            k1 -= 1
        if i == "}":
            k2 -= 1
        if i == "]":
            k3 -= 1
        if i == ">":
            k4 -= 1
    return k1 == 0 and k2 == 0 and k3 == 0 and k4 == 0


print(string1 + " - string")
print("string is correct" if check(string1) else "string is wrong")

print("4")
string2 = "1 1 1 1 1 1 1"
print(string2 + "  start string")
print (re.sub('1 1', '1 0', string2) + "  final sting")

print("5")
first_number = "123456789"


def create_number(number):
    num = 0
    q = 0
    for i in number:
        s = int(i)
        if s % 2 == 0 and q < 1:
            num += s
            q += 1
        elif s % 2 == 0 and (q >= 1 or q > 1):
            num *= 10
            num += s
            q += 1

    return num


print (first_number + " - starting number")
print create_number(first_number)
